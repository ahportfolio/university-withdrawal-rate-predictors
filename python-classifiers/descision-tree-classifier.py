__author__ = 'aston'

import csv
import random
import pydot
from sklearn import tree
from sklearn.externals.six import StringIO
from datetime import datetime


def load_data(file_path, starting_record=0, debug=False, delimiter=','):
    loaded_data = []
    with open(file_path) as csvfile:
        data_reader = csv.reader(csvfile, delimiter=delimiter)
        COLUMNS = []
        i = -1
        for row in data_reader:
            i += 1
            if i == 0:
                COLUMNS = row
                continue

            if i < starting_record:
                print('Skipping row {0}'.format(i))
                continue

            row_dict = dict(zip(COLUMNS, row))

            loaded_data.append(row_dict)

            if debug:
                print('data imported at row {0}'.format(i))
    return loaded_data


def metrics_calculate_f1_score(actual, predicted):
    def _bitand(x, y):
        return [1 if (x[i] == 1 and y[i] == 1) else 0 for i in xrange(len(x))]

    def _not(x):
        return [1 if x[i] == 0 else 0 for i in xrange(len(x))]

    tp = sum(_bitand(actual, predicted))
    fp = sum(_bitand(_not(actual), predicted))
    tn = sum(_bitand(_not(actual), _not(predicted)))
    fn = sum(_bitand(actual, _not(predicted)))

    if tp != 0:
        prec = tp / float(tp + fp)
        rec = tp / float(tp + fn)

        f1 = (2 * prec * rec) / float(prec + rec)
    else:
        prec = 0

        rec = 0

        f1 = 0

    return (f1, prec, rec, tp, fp, tn, fn)


def main():
    DATA_FEATURES_FILE_LOCATION = '../data/class-1-fixed-features.csv'
    DATA_TARGETS_FILE_LOCATION = '../data/class-1-fixed-targets.csv'
    DATASET_FILE_LOCATION = '../data/class-1-fixed-dataset.csv'

    LEARNT_TREE_FILE_LOCATION = '../data/tree-{0}.pdf'.format(
        datetime.now().isoformat())
    SCRIPT_DEBUG = False

    dataset = load_data(DATASET_FILE_LOCATION, debug=SCRIPT_DEBUG)
    random.shuffle(dataset)

    feature_properties = ["Gender", "Age", "Year 1 GPA",
                          "Learning Progress Index", "Average Course Grade",
                          "High School Ability Index", "Level 1 Average Grade",
                          "Level 2 Average Grade", "Years In School"]
    target_property = "Is Withdrawn"

    features = [[float(x[y]) for y in feature_properties] for x in dataset]
    targets = [int(x[target_property]) for x in dataset]

    test_size = validation_size = int(len(features) * .2)

    training_set = {
        'features': features[:-1 * (test_size + validation_size)],
        'targets': targets[:-1 * (test_size + validation_size)]
    }

    validation_set = {
        'features': features[-(validation_size + test_size):-test_size],
        'targets': targets[-(validation_size + test_size):-test_size]
    }

    test_set = {
        'features': features[-test_size:],
        'targets': targets[-test_size:]
    }

    clf = tree.DecisionTreeClassifier(max_depth=4, max_leaf_nodes=4)
    clf = clf.fit(training_set['features'], training_set['targets'])

    training_predictions = clf.predict(training_set['features'])
    validation_predictions = clf.predict(validation_set['features'])
    test_predictions = clf.predict(test_set['features'])

    def _dump_evaluation_metrics(actual, predicted):
        (f1, prec, rec, tp, fp, tn, fn) = \
            metrics_calculate_f1_score(actual, predicted)

        print "Accuracy {0}".format((tp + tn) / float(tp + fp + tn + fn))
        print "Precision {0}".format(prec)
        print "Recall {0}".format(rec)
        print "F1 Score {0}".format(f1)

    print "Training Set Results:"
    _dump_evaluation_metrics(training_set['targets'], training_predictions)

    print "\nCross Validation Set Results:"
    _dump_evaluation_metrics(validation_set['targets'], validation_predictions)

    print "\nTest Set Results:"
    _dump_evaluation_metrics(test_set['targets'], test_predictions)

    dot_data = StringIO()
    tree.export_graphviz(clf, out_file=dot_data)
    graph = pydot.graph_from_dot_data(dot_data.getvalue())
    graph.write_pdf(LEARNT_TREE_FILE_LOCATION)


if __name__ == '__main__':
    main()
