University Withdrawal Rate Classifiers
-----------------------------------------

This repository maintains the code for the Machine Learning classifiers that I implemented
to perform the experiments for my MSc dissertation. The code for the following tools are included:

- Artificial Neural Network
- Logistic Regression Classifier
- Support Vector Machine
- KMeans Cluster Builder
- Decision Tree Classifier

Please note: The data used to train the models is not openly available so I am not able to share it in this repository

Background:

The purpose of education has been the focus of many discussions with philosophers such as Aristotle, Plato and 
Confucius having presented their perspectives, which had been rooted in the belief that education is a basic human 
right and as such, educators have a societal responsibility to convey their knowledge. Undeniably however, 
one of the primary goals of modern universities is to maximize their efficiency, and consequently minimize 
their losses at the end of the fiscal year. A university's graduation rate is one of the more popular measures 
of its efficiency, and it is usually expressed as the percentage of their freshmen cohort who complete their 
program in the expected time. My study focused on exploratory analysis and using predictive modeling techniques guided by
an IKDDM process model. Through my study I was able to successfully identify a set of predictors for early student withdrawal,
derived from a dataset of student records obtained from my University.
The predictors were demonstrated to be optimal by using them to train a set of Supervised Learning Algorithms 
(Logistic Regression, Support Vector Machine, Artificial Neural Network, Decision Tree) to achieve an accuracy of 
over 70% at predicting whether or not a given student from the dataset withdrew early from their degree program. 
The study then used multiple performance measures to select the best classifier.