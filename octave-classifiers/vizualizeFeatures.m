clear; close all;
[X, Y] = loadData();

figure;
utilVisualizeFeatureComparison(X, Y, 1);
legend("Decided Not To Withdraw   ", "Decided To Withdraw         ");
title("Histogram showing the distribution of the Students' Average CSEC Level Assessment");
xlabel('Average CSEC Level Assessment');
ylabel('Number of Students');
print -dpng -color 'printed/Average CSEC Level Assessment.png'



figure;
utilVisualizeFeatureComparison(X, Y, 2);
legend("Decided Not To Withdraw   ", "Decided To Withdraw         ");
title("Histogram showing the distribution of the Students' High School Rating");
xlabel('High School Rating');
ylabel('Number of Students');
print -dpng -color 'printed/High School Rating.png'



figure;
utilVisualizeFeatureComparison(X, Y, 3);
legend("Decided Not To Withdraw   ", "Decided To Withdraw         ");
title("Histogram showing the distribution of the Students' Number of Years In School");
xlabel('Number of Years In School');
ylabel('Number of Students');
print -dpng -color 'printed/Number of Years In School.png'



figure;
utilVisualizeFeatureComparison(X, Y, 5);
legend("Decided Not To Withdraw   ", "Decided To Withdraw         ");
title("Histogram showing the distribution of the Students' Age");
xlabel('Age');
ylabel('Number of Students');
print -dpng -color 'printed/Age.png'



figure;
utilVisualizeFeatureComparison(X, Y, 6);
legend("Decided Not To Withdraw   ", "Decided To Withdraw         ");
title("Histogram showing the distribution of the Students' First Year GPA");
xlabel('First Year GPA');
ylabel('Number of Students');
print -dpng -color 'printed/First Year GPA.png'



figure;
utilVisualizeFeatureComparison(X, Y, 7);
legend("Decided Not To Withdraw   ", "Decided To Withdraw         ");
title("Histogram showing the distribution of the Students' Average Overall Course Grade");
xlabel('Average Overall Course Grade');
ylabel('Number of Students');
print -dpng -color 'printed/Average Overall Course Grade.png'



figure;
utilVisualizeFeatureComparison(X, Y, 8);
legend("Decided Not To Withdraw   ", "Decided To Withdraw         ");
title("Histogram showing the distribution of the Students' Progress Index");
xlabel('Progress Index');
ylabel('Number of Students');
print -dpng -color 'printed/Progress Index.png'



figure;
utilVisualizeFeatureComparison(X, Y, 9);
legend("Decided Not To Withdraw   ", "Decided To Withdraw         ");
title("Histogram showing the distribution of the Students' Average CAPE Level Assessment");
xlabel('Average CAPE Level Assessment');
ylabel('Number of Students');
print -dpng -color 'printed/Average CAPE Level Assessment.png'


