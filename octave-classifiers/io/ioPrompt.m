function ioPrompt(prompt, varargin)
    printf(["\n", prompt], varargin{:});

    pause;
end;
