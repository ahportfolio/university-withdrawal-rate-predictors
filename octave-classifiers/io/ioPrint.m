function ioPrint(format, varargin)
    formatted = sprintf(format, varargin{:});
    printf(formatted);

    fflush(stdout);
end;
