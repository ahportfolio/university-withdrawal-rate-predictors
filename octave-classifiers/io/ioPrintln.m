function ioPrintln(format, varargin)
    formatted = sprintf([format "\n"], varargin{:});
    printf(formatted);

    fflush(stdout);
end;
