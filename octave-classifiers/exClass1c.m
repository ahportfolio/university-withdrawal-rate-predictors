clear; close all; clc;

bootstrap();

function [K, X, Y] = loadClass1Data()
    K = csvread('../data/class-1-fixed-identifiers.csv');
    X = csvread('../data/class-1-fixed-features.csv');
    Y = csvread('../data/class-1-fixed-targets.csv');

    K = K(2:end, :); % remove title row
    X = X(2:end, :); % remove title row
    Y = Y(2:end, :); % remove title row

%    data = csvread('../data/ex2data2.txt');
%
%    X = data(:, [1, 2]); Y = data(:, 3);

%    load('ex6data3.mat');
%    Y = y;
end

[K, xOrig, Y] = loadClass1Data();

X = xOrig;

r = metricsCalculateFeatureRatings(X, Y);
[topRatings, topFeatures] = sort(r, 'descend');

selectedFeatures = topFeatures(1:end);
%selectedFeatures = [1, 2, 6, 7];
ioPrintln('Selecting top %d features: %s', size(selectedFeatures, 2), int2str(selectedFeatures));
X = X(:, selectedFeatures);

mOriginal = size(X, 1);
nOriginal = size(X, 2);

ioPrintln('Data imported: %d rows, %d features', mOriginal, nOriginal);

classifier = SVMClassifier(X, Y, [15]);

classifier = shuffleDataset(classifier);

%visualizeData(classifier);

classifier = normalizeFeatures(classifier);

%classifier = expandFeatures(classifier, 2);

ioPrintln('Expanded imported data: %d rows, %d features, %d classes', getTrainingSetRowCount(classifier), getTrainingSetFeatureCount(classifier), getClassesCount(classifier));

classifier = segmentValidationSet(classifier);

classifier = segmentTestSet(classifier);

%classifier = enableLinearKernel(classifier);

ioPrintln('Training set:         %d rows, %d features', getTrainingSetRowCount(classifier), getTrainingSetFeatureCount(classifier));
ioPrintln('Cross validation set: %d rows, %d features', getValidationSetRowCount(classifier), getValidationSetFeatureCount(classifier));
ioPrintln('Test set:             %d rows, %d features', getTestSetRowCount(classifier), getTestSetFeatureCount(classifier));

ioPrintln('\nCalculating optimal C and sigma');
classifier = calculateOptimalParameters(classifier);

ioPrintln('Calculating parameters');
classifier = buildParameters(classifier, true);

ioPrintln("\nTraining Set Evaluation");
utilDumpPredictionEvalMetrics(getTrainingSetPredictions(classifier), getTrainingSetTarget(classifier));

ioPrintln("\nCross Validation Set Evaluation");
utilDumpPredictionEvalMetrics(getValidationSetPredictions(classifier), getValidationSetTarget(classifier));

ioPrintln("\nTest Set Evaluation");
utilDumpPredictionEvalMetrics(getTestSetPredictions(classifier), getTestSetTarget(classifier));

ioPrintln('\nPlotting learning curve');
visualizeLearningCurve(classifier);

ioPrintln('\nPlotting decision boundary');
%visualizeDecisionBoundary(classifier);

%ioPrintln('\nPlotting prediction probabilities');
%visualizeProbabilityDensity(classifier);

