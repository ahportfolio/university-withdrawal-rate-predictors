function c = pruneRedundantFeatures(c)

    mu = mean(c.xTrain);
    c.xTrain = c.xTrain(:, find(mu != 0));
    c.xVal = c.xVal(:, find(mu != 0));
    c.xTest = c.xTest(:, find(mu != 0));

    sigma = std(c.xTrain);
    c.xTrain = c.xTrain(:, find(sigma != 0));
    c.xVal = c.xVal(:, find(sigma != 0));
    c.xTest = c.xTest(:, find(sigma != 0));

end
