function visualizeLearningCurve(c)

	m = size(c.xTrain, 1);
	n = size(c.xTrain, 2);

	error_train = zeros(m, 1);
	error_val   = zeros(m, 1);

	for i = 5:m
%		ioPrintln('Calculating error with %d examples. ', i);
		trainedWeights = sCalculateParameters(c, c.xTrain(1:i, :), c.yTrain(1:i), c._classifierMargin, c._kernel, c._kernelOptions);
		[error_train(i), gradient] = sCostFunction(c, c.xTrain(1:i, :), c.yTrain(1:i), trainedWeights);
%		ioPrint('Training Error: %f ', error_train(i));
		[error_val(i), gradient] = sCostFunction(c, c.xVal, c.yVal, trainedWeights);
%		ioPrintln('Validation Error: %f', error_val(i));
	end

	figure;
	plot(1:m, error_train, 1:m, error_val);
	ylim([0,1]);

	title(sprintf('Learning Curve (C = %f, sigma = %f)', c._classifierMargin, c._kernelOptions.sigma));

	xlabel('Number of training examples')
	ylabel('Error')

	legend('Train	', 'Cross Validation	');

	drawnow;
end
