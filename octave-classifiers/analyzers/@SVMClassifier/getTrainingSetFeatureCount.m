function count = getTrainingSetFeatureCount(c)
    count = size(c.xTrain, 2);
end
