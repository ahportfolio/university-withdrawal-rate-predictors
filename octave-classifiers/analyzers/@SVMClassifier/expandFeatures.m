function c = expandFeatures(c, maxDegree)
    [c.xTrain, c._builtTerms] = utilBuildPolynomialTerms(c.xTrain, maxDegree);

    c.xVal = c.xTrain;
    c.xTest = c.xTrain;


    c._featureExpansionDegree = maxDegree;
end
