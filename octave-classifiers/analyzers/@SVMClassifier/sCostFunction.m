function [J, grad] = sCostFunction(_, X, Y, model)
    m = length(Y); % number of training examples

    predictions = sCalculatePredictions(_, X, model);

    J = mean(double(predictions ~= Y));

    grad = [NaN];
end
