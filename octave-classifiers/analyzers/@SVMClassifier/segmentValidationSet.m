function c = segmentValidationSet(c)

    m = size(c.xTrain, 1);

    validationSetSize = floor(size(c.xOriginal, 1) * 0.2);

    c.xVal = c.xTrain(((m-validationSetSize) + 1):end, :);
    c.yVal = c.yTrain(((m-validationSetSize) + 1):end, :);

    c.xTrain = c.xTrain(1:(m-validationSetSize), :);
    c.yTrain = c.yTrain(1:(m-validationSetSize), :);

    %sum(sum(c.xOriginal != [c.xTrain(:, 2:6);c.xTest(:, 2:6);c.xVal(:, 2:6)]))
end
