function visualizeProbabilityDensity(c)
	trainingDataProbability = sCalculatePredictions(c, c.xTrain, c.weights, true);
	validationDataProbability = sCalculatePredictions(c, c.xVal, c.weights, true);
	testDataProbability = sCalculatePredictions(c, c.xTest, c.weights, true);

    figure;
    hist(trainingDataProbability(find(c.yTrain==0), 1));
    title("Probability Distribution of Training Data Predictions")
    hold on;
    hist(trainingDataProbability(find(c.yTrain==1), 1), "facecolor", "r");
	xlim([0, 1]);

    legend("Value = 0   ", "Vale = 1    ");
    hold off;

    figure;
    hist(validationDataProbability(find(c.yVal==0), 1));
    title("Probability Distribution of Cross Validation Data Predictions")
    hold on;
    hist(validationDataProbability(find(c.yVal==1), 1), "facecolor", "r");
	xlim([0, 1]);

    legend("Value = 0   ", "Vale = 1    ");
    hold off;

    figure;
    hist(testDataProbability(find(c.yTest==0), 1));
    title("Probability Distribution of Test Data Predictions")
    hold on;
    hist(testDataProbability(find(c.yTest==1), 1), "facecolor", "r");

	xlim([0, 1]);
    legend("Value = 0   ", "Vale = 1    ");
    hold off;
end
