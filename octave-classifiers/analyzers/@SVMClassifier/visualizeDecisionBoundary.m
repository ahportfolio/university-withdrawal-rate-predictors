function visualizeDecisionBoundary(c)
    % PLOTDECISIONBOUNDARY Plots the data points X and Y into a new figure with
    % the decision boundary defined by theta
    %   PLOTDECISIONBOUNDARY(theta, X,Y) plots the data points with + for the
    %   positive examples and o for the negative examples. X is assumed to be
    %   a either
    %   1) Mx3 matrix, where the first column is an all-ones column for the
    %      intercept.
    %   2) MxN, N>3 matrix, where the first column is all-ones

    % Plot Data
    [U, X_2d, Y_2d] = visualizeData(c);

    model = c.model;
    X = X_2d;
    Y = Y_2d;

    model.X = utilPCAReduce(model.X, NaN, U);

    if strcmp(model.kernelName, 'linearKernel')
        w = model.w;
        b = model.b;
        xp = linspace(min(X(:,1)), max(X(:,1)), 100);
        yp = - (w(1)*xp + b)/w(2);
        hold on;
        plot(xp, yp, '-b');
        hold off;
    else
        % Make classification predictions over a grid of values
        x1plot = linspace(min(X(:,1)), max(X(:,1)), 100)';
        x2plot = linspace(min(X(:,2)), max(X(:,2)), 100)';
        [X1, X2] = meshgrid(x1plot, x2plot);
        vals = zeros(size(X1));
        for i = 1:size(X1, 2)
           this_X = [X1(:, i), X2(:, i)];

           vals(:, i) = sCalculatePredictions(c, this_X, model);
        end

        % Plot the SVM boundary
        hold on;
        contour(X1, X2, vals);
        hold off;
    end
end
