function [model] = sCalculateParameters(_, X, Y, classifierMargin, kernelCode, options, debug=false)

    if strcmp(kernelCode, 'gaussian')
        kernel = @(X1, X2) sCalculateGaussianKernelResult(_, X1, X2, options.sigma);
        kernelName = 'gaussianKernel';
    else
        kernel = @(X1, X2) sCalculateLinearKernelResult(_, X1, X2);
        kernelName = 'linearKernel';
    end

    model = sTrainSVM(_, X, Y, classifierMargin, kernel, options.tolerance, options.maxIters, kernelName);
end
