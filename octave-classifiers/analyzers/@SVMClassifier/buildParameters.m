function c = buildParameters(c, debug=false)

    [c.model] = sCalculateParameters(c, c.xTrain, c.yTrain, c._classifierMargin, c._kernel, c._kernelOptions, debug);
end
