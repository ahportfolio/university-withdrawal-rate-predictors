function c = normalizeFeatures(c)

    m = size(c.xTrain, 1); % number of rows
    n = size(c.xTrain, 2); % number of features

    c.mu = mean(c.xTrain);
    c.sigma = max(c.xTrain) - min(c.xTrain);

    c.xTrain = ((c.xTrain - repmat(c.mu, m, 1)) ./ repmat(c.sigma, m, 1));

    c.xVal = c.xTrain;
    c.xTest = c.xTrain;
end
