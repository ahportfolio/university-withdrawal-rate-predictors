function c = calculateOptimalParameters(c)

    function [C, sigma] = trainLowestError(X, y, Xval, yval)
        steps = [.01, .03, .1, .3, 1.3, 1, 10, 30];

        lowestError = 'x-first-run';
        lowestErrorParameters = zeros(2);

        for C=steps
        for sigma=steps
            printf('\nTraining with C=%.2f, and sigma=%.2f', C, sigma);
            model = sCalculateParameters(c, X, y, C, c._kernel, c._kernelOptions);
            predictions = sCalculatePredictions(c, Xval, model);

            currentError = mean(double(predictions ~= yval));

            if (lowestError == 'x-first-run' || currentError < lowestError)
                lowestErrorParameters = [C, sigma];
                lowestError = currentError;
            end;

            printf('\nTrained with C=%.2f, and sigma=%.2f, error=%.2f, current lowest error=%.2f, current lowest parameters=%.2f,%.2f', C, sigma, currentError, lowestError, lowestErrorParameters(1), lowestErrorParameters(2));

        endfor;
        endfor;

        printf('\nLowest Error error=%.2f, lowest error parameters=%.2f, %.2f', lowestError, lowestErrorParameters(1), lowestErrorParameters(2));

        C = lowestErrorParameters(1);
        sigma = lowestErrorParameters(2);
    end

%    [C, sigma] = trainLowestError(c.xTrain, c.yTrain, c.xVal, c.yVal);

    C = 1; sigma = 0.1;

    c._classifierMargin = C;
    c._kernelOptions.sigma = sigma;
end
