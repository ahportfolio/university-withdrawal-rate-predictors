function c = SVMClassifier(X, Y)
    c.xOriginal = X;
    c.yOriginal = Y;

    c.xTrain = X;
    c.yTrain = Y;

    c.xVal = X;
    c.yVal = Y;

    c.xTest = X;
    c.yTest = Y;

    c._featureExpansionDegree = -1;

    c.model = NaN;

    c._classesCount = sum(unique(Y) != 0);

    c._classifierMargin = 1;

    c._kernel = 'gaussian';
    c._kernelOptions.sigma = .1;
    c._kernelOptions.tolerance = 1e-3;
    c._kernelOptions.maxIters = 20;

    c = class(c, "SVMClassifier");
end
