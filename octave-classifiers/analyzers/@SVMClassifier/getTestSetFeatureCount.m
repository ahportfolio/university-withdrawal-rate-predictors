function count = getTestSetFeatureCount(c)
    count = size(c.xTest, 2);
end
