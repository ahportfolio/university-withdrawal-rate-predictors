function predictions = sCalculatePredictions(_, X, model, getProbabilities=false)
%    keyboard('debug> ');
    predictions = sPredictSVM(_, model, X);
end
