function visualizeCostCurve(c)
    % Plot the convergence graph
    figure;
    plot(1:numel(c._costHistory), c._costHistory, '-b', 'LineWidth', 2);
    title('Cost Function Curve');
    xlabel('Number of iterations');
    ylabel('Cost J');

    drawnow;
end
