function [J, grad] = sCostFunction(_, X, Y, unrolledWeights, lambda, architecture)

    L = length(architecture);
    K = architecture(:, end);
    m = size(X, 1);

    weights = sInflateUnrolledWeights(_, unrolledWeights, architecture);

    % return values
    J = 0;
    gradients = {};

    activationValueSet = {};
    zValueSet = {};

    activationValues = ctranspose([ones(m, 1) X]);
    activationValueSet{1} = activationValues;
    zValueSet{1} = NaN;


    for l = 2:L
        zValues = (weights{l-1} * activationValues);
        activationValues = sCalculateSigmoid(_, zValues);

        if l != L
            activationValues = [ones(1, size(activationValues, 2)); activationValues];
        end

        activationValueSet{l} = activationValues;
        zValueSet{l} = zValues;
    end

%    ioPrint("Calculating cost ... ");
    predictions = activationValues;

    Yk = repmat(ctranspose(Y), K, 1) == repmat(ctranspose([1:K]), 1, m);

    J = sum(sum(-Yk .* log(predictions) - ((ones(K, m) - Yk) .* log(ones(K, m) - predictions)))) / m;

    for l = 1:(L-1)
        J += sum(sum(weights{l}(:, 2:end) .^ 2))  * (lambda / (2 * m));
    end

%    ioPrintln("Cost calculated: %f, lambda: %f", J, lambda);

    deltas = {};
    deltas{L} = predictions - Yk;
    deltas{1} = NaN;
    for l = [(L - 1):-1:2]
        deltas{l} = (ctranspose(weights{l}(:, 2:end)) * deltas{l + 1}) .* sCalculateInverseSigmoid(_, zValueSet{l});
    end

    Deltas = {};
    for l = 1:(L-1)
        Deltas{l} = (deltas{l + 1} * ctranspose(activationValueSet{l}));
    end


%    keyboard('debug33> ');

    % Unroll gradients
    grad = [];
    for l = 1:(L-1)
        currentWeights = weights{l};
        grad = [grad ; ((Deltas{l}  + (lambda .* [zeros(size(currentWeights, 1), 1) currentWeights(:, 2:end)])) ./ m)(:)];
    end
end
