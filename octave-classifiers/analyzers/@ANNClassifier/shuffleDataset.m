function c = shuffleDataset(c)

    m = size(c.xTrain, 1);
    [shuffledRows] = randperm(m);


    c.xTrain = c.xTrain(shuffledRows, :);
    c.yTrain = c.yTrain(shuffledRows, :);

    c.xVal = c.xTrain;
    c.yVal = c.yTrain;

    c.xTest = c.xTrain;
    c.yTest = c.yTrain;

end
