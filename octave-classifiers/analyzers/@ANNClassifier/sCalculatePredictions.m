function predictions = sCalculatePredictions(_, X, weights, getProbabilities=false)
    L = size(weights, 2) + 1;
    m = size(X, 1);
    K = size(weights{end}, 1);

    activationValues = ctranspose([ones(m, 1) X]);
    for l = 2:L
        zValues = weights{l - 1} * activationValues;
        activationValues = sCalculateSigmoid(_, zValues);
        if l != L
            activationValues = [ones(1, m); activationValues];
        end
    end

    predictions = activationValues;

%    keyboard('debug> ');

    if !getProbabilities
        if K == 1
            predictions = predictions > .5;
        else
            [dummy, predictions] = max(predictions);
        end
    end

    predictions = ctranspose(predictions);
end
