function predictions = getTrainingSetPredictions(c)
    predictions = sCalculatePredictions(c, c.xTrain, c.weights);
end
