function weights = sInflateUnrolledWeights(_, unrolledWeights, architecture)

    L = length(architecture);

    weights = {};
    nextWeightElementPosition = 1;
    for l = 2:L
        fanIn = architecture(l-1);
        fanOut = architecture(l);
        currentWeightElementCount = fanOut * (fanIn + 1);
        unrolledElements = unrolledWeights(nextWeightElementPosition:(nextWeightElementPosition + currentWeightElementCount) - 1, :);
        weights{l-1} = reshape(unrolledElements, fanOut, (fanIn + 1));

        nextWeightElementPosition += currentWeightElementCount;
    end
end
