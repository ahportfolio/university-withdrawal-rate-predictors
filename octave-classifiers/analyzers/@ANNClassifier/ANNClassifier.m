function c = ANNClassifier(X, Y, hiddenLayers)
    c.xOriginal = X;
    c.yOriginal = Y;

    c.xTrain = X;
    c.yTrain = Y;

    c.xVal = X;
    c.yVal = Y;

    c.xTest = X;
    c.yTest = Y;

    c._featureExpansionDegree = -1;

    c.weights = {};

    K = sum(unique(Y) != 0);

    n = size(X, 2);

    architecture = [n hiddenLayers K];

    c._architecture = architecture;

    c._optimizer = 'fminunc';
    c._optimizerOptions.maxIters = 50;
    c._optimizerOptions.regularizationFactor = .3;

    c = class(c, "ANNClassifier");


    for i = 2:length(architecture)
        c.weights{i-1} = sBuildInitialWeights(c, c._architecture(i-1), c._architecture(i));
    end
end
