function [theta, J_History] = sCalculateParameters(_, X, Y, theta, optimizer, options, architecture, debug=false)

    lambda = options.regularizationFactor;
    numIterations = options.maxIters;
    J_History = zeros(numIterations, 1);

    if strcmp(optimizer, 'grad-desc')
        % Gradient decent

%        keyboard('debug> ');

        alpha = options.learningRate;

        for i = 1:numIterations

            [cost, grad] = sCostFunction(_, X, Y, theta, lambda, architecture);

            theta = theta - (alpha * grad);

            J_History(i) = cost;

            if debug
                ioPrintln('Gradient descent iteration: %d %f', i, cost);
            end
        end
    else
        % Optimization function
        options2 = optimset('GradObj', 'on', 'MaxIter', numIterations);
        [theta, J, exit_flag] = ...
            fminunc(@(t)(sCostFunction(_, X, Y, t, lambda, architecture, debug)), theta, options2);
    end
end
