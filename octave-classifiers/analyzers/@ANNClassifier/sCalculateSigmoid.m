function g = sCalculateSigmoid(_, z)
    inputSize = size(z);
    g = ones(inputSize) ./ (ones(inputSize) + exp(-z));
end
