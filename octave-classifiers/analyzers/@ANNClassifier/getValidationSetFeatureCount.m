function count = getValidationSetFeatureCount(c)
    count = size(c.xVal, 2);
end
