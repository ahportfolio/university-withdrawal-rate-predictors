function predictions = getTestSetPredictions(c)
    predictions = sCalculatePredictions(c, c.xTest, c.weights);
end
