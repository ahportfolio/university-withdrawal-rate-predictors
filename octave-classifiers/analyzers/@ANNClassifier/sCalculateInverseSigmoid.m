function g = sCalculateInverseSigmoid(_, z)

    g = sCalculateSigmoid(_, z) .* (ones(size(z)) - sCalculateSigmoid(_, z));

end
