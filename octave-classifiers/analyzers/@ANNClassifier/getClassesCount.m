function count = getClassesCount(c)
    count = size(c.weights(end), 1);
end
