function c = expandFeatures(c, maxDegree)
    [c.xTrain, c._builtTerms] = utilBuildPolynomialTerms(c.xTrain, maxDegree);

    c.xVal = c.xTrain;
    c.xTest = c.xTrain;


    c._featureExpansionDegree = maxDegree;


    n = size(c.xTrain, 2);

    architecture = [n c._architecture(:, 2:end)];

    c._architecture = architecture;

    for i = 2:length(architecture)
        c.weights{i-1} = sBuildInitialWeights(c, c._architecture(i-1), c._architecture(i));
    end
end
