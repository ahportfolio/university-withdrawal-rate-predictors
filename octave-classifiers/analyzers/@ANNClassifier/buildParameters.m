function c = buildParameters(c, debug=false)

    architecture = c._architecture;

    L = length(architecture);

    theta = [];
    for l = 1:(L - 1)
        theta = [theta; c.weights{l}(:)];
    end


    [unrolledWeights, c._costHistory] = sCalculateParameters(c, c.xTrain, c.yTrain, theta, c._optimizer, c._optimizerOptions, architecture, debug);

    c.weights = sInflateUnrolledWeights(c, unrolledWeights, architecture);
end
