function visualizeDecisionBoundary(c)
    % PLOTDECISIONBOUNDARY Plots the data points X and Y into a new figure with
    % the decision boundary defined by theta
    %   PLOTDECISIONBOUNDARY(theta, X,Y) plots the data points with + for the
    %   positive examples and o for the negative examples. X is assumed to be
    %   a either
    %   1) Mx3 matrix, where the first column is an all-ones column for the
    %      intercept.
    %   2) MxN, N>3 matrix, where the first column is all-ones

    % Plot Data
    U = visualizeData(c);

    hold on;

    % Here is the grid range
    u = linspace(-1, 1.5, 50);
    v = linspace(-1, 1.5, 50);

    z = zeros(length(u), length(v));

    ioPrint("Calculating decision boundary: ");
    % Evaluate z = theta * x over the grid
    for i = 1:length(u)

        percentage = (i / length(u)) * 100;
        ioPrint(' %d%s', percentage, '%%');
        for j = 1:length(v)
            mappedVector = [u(i), v(j)];
            expandedVector = utilPCAExpansion(mappedVector, U);

            if c._featureExpansionDegree > -1
                z(i,j) = sCalculatePredictions(c, utilBuildPolynomialTerms(expandedVector, c._featureExpansionDegree), c.weights, true);
            else
                z(i,j) = sCalculatePredictions(c, expandedVector, c.weights, true);
            end

%            z(i,j) = sCalculateInverseSigmoid(c, z(i,j));
        end
    end
    ioPrint("\n");

    z = ctranspose(z); % important to transpose z before calling contour

%    keyboard('debug7777> ');

    figure;
    % Plot z = 0
    % Notice you need to specify the range [0, 0]
    mesh(u, v, z, 'LineWidth', 2)
    drawnow;

    hold off;

end
