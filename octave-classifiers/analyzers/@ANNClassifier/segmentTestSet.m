function c = segmentTestSet(c)

    m = size(c.xTrain, 1);

    testSetSize = floor(size(c.xOriginal, 1) * 0.2);

    c.xTest = c.xTrain(((m-testSetSize) + 1):end, :);
    c.yTest = c.yTrain(((m-testSetSize) + 1):end, :);

    c.xTrain = c.xTrain(1:(m-testSetSize), :);
    c.yTrain = c.yTrain(1:(m-testSetSize), :);

    %sum(sum(c.xOriginal != [c.xTrain(:, 2:6);c.xTest(:, 2:6);c.xVal(:, 2:6)]))
end
