function visualizeLearningCurve(c)

	m = size(c.xTrain, 1);
	n = size(c.xTrain, 2);

	error_train = zeros(m, 1);
	error_val   = zeros(m, 1);

	lambda = c._optimizerOptions.regularizationFactor;

	newWeights = {};


    for i = 2:length(c._architecture)
        newWeights{i-1} = sBuildInitialWeights(c, c._architecture(i-1), c._architecture(i));
    end

    theta = [];
    for l = 1:(length(c._architecture) - 1)
        theta = [theta; newWeights{l}(:)];
    end

	for i = 1:m
		trainedWeights = sCalculateParameters(c, c.xTrain(1:i, :), c.yTrain(1:i), theta, c._optimizer, c._optimizerOptions, c._architecture);
		[error_train(i), gradient] = sCostFunction(c, c.xTrain(1:i, :), c.yTrain(1:i), trainedWeights, lambda, c._architecture);
		[error_val(i), gradient] = sCostFunction(c, c.xVal, c.yVal, trainedWeights, lambda, c._architecture);
	end


	figure;
	plot(1:m, error_train, 1:m, error_val);
	ylim([0,1]);


	title(sprintf('Learning Curve (lambda = %f)', lambda));

	xlabel('Number of training examples')
	ylabel('Cost')

	legend('Train	', 'Cross Validation	');

	drawnow;
end
