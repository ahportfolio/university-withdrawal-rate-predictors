function U = visualizeData(self)

    % Create New Figure
    figure; hold on;

    K = size(self.centroids, 1);
    n = size(self.xOriginal, 2);

    idx = self.clusterAssignments;
    if n > 2
        [X, U] = utilPCAReduce(self.xOriginal, 2);
        title(sprintf('Dataset visualization mapped to 2D from %dD', n));

        ylabel('Z1');
        xlabel('Z2');
    else
        title(sprintf('Dataset visualization', n));

        U = eye(n);
        X = self.xOriginal;

        ylabel('X1');
        xlabel('X2');
    end

    % Create palette
    palette = hsv(K + 1);
    colors = palette(idx, :);

    % Plot the data
    scatter(X(:,1), X(:,2), 15, colors);

%    legendValues = {1:K};
%    for i = 1:K
%        legendValues(i) = ['Cluster ' int2str(i), '    '];
%    end
%
%    legend(legendValues);

    hold off;
    drawnow;
end
