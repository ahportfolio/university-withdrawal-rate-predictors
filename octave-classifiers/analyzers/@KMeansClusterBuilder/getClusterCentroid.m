function centroid = getClusterCentroid(self, i)
%    keyboard('debug 2> ');
    centroid = ctranspose((self.centroids(i, :).* self.sigma) + self.mu);
end
