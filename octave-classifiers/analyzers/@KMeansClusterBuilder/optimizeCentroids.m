function self = optimizeCentroids(self, debug=false)

    function drawLine(p1, p2, varargin)
        plot([p1(1) p2(1)], [p1(2) p2(2)], varargin{:});
    end

    function plotDataPoints(X, idx, K)
        % Create palette
        palette = hsv(K + 1);
        colors = palette(idx, :);

        % Plot the data
        scatter(X(:,1), X(:,2), 15, colors);
    end

    function plotProgresskMeans(X, centroids, previous, idx, K, i)

        % Plot the examples
        plotDataPoints(X, idx, K);

        % Plot the centroids as black x's
        plot(centroids(:,1), centroids(:,2), 'x', ...
             'MarkerEdgeColor','k', ...
             'MarkerSize', 10, 'LineWidth', 3);

        % Plot the history of the centroids with lines
        for j=1:size(centroids,1)
            drawLine(centroids(j, :), previous(j, :));
        end

        % Title
        title(sprintf('Iteration number %d', i));
    end

    function idx = findClosestCentroids(X, centroids)
        K = size(centroids, 1);

        idx = zeros(size(X,1), 1);

        distances = [];
        for i=1:K
            distances = [distances sum((X - repmat(centroids(i, :), size(X, 1), 1)) .^ 2, 2)];
        end;

        [v, idx] = min(distances, [], 2);
    end

    function centroids = computeCentroids(X, idx, K)
        [m n] = size(X);

        centroids = zeros(K, n);

        for i=1:K
            assignedPoints = sum(idx == i, 1);

            if assignedPoints > 0
                centroids(i,:) = sum(X .* repmat((idx == i), 1, n))/sum(idx == i, 1);
            end
        end;
    end


    plot_progress = debug;

    X = self.xTrain;
    initial_centroids = self.centroids;

    max_iters = 10;


    % Set default value for plot progress
    if ~exist('plot_progress', 'var') || isempty(plot_progress)
        plot_progress = false;
    end

    % Plot the data if we are plotting progress
    if plot_progress
        figure;
        hold on;
    end

    % Initialize values
    [m n] = size(X);
    K = size(initial_centroids, 1);
    centroids = initial_centroids;
    previous_centroids = centroids;
    idx = zeros(m, 1);

    % Run K-Means
    for i=1:max_iters

        % Output progress
        fprintf('K-Means iteration %d/%d...\n', i, max_iters);
        if exist('OCTAVE_VERSION')
            fflush(stdout);
        end

        % For each example in X, assign it to the closest centroid
        idx = findClosestCentroids(X, centroids);

        % Optionally, plot progress here
        if plot_progress
            plotProgresskMeans(X, centroids, previous_centroids, idx, K, i);
            previous_centroids = centroids;
        end

        % Given the memberships, compute new centroids
        centroids = computeCentroids(X, idx, K);
    end

    % Hold off if we are plotting progress
    if plot_progress
        hold off;
    end

    self.centroids = centroids;
    self.clusterAssignments = idx;

end
