function count = getClusterCount(self)
    count = size(self.centroids, 1);
end
