function self = randomizeCentroids(self)

    X = self.xTrain;
    K = size(self.centroids, 1);

    randIdx = randperm(size(X, 1));
    self.centroids = X(randIdx(1:K), :);

end
