function self = KMeansClusterBuilder(xOriginal, centroidCount)
    self.xOriginal = xOriginal;
    self.xTrain = xOriginal;
    self.clusterAssignments = ones(size(self.xOriginal, 1), 1);
    self.centroids = ones(centroidCount, size(self.xOriginal, 2));

    self = class(self, 'KMeansClusterBuilder');
end
