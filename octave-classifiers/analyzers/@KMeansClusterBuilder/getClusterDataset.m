function X_cluster = getClusterDataset(self, idx)
    X_cluster = self.xOriginal(self.clusterAssignments == idx, :);
end
