function [theta, J_History] = sCalculateParameters(_, X, Y, theta, optimizer, options, debug=false)

    lambda = options.regularizationFactor;
    numIterations = options.maxIters;
    J_History = zeros(numIterations, 1);

    if strcmp(optimizer, 'grad-desc')
        % Gradient decent

%        keyboard('debug> ');

        alpha = options.learningRate;

        for i = 1:numIterations

            [cost, grad] = sCostFunction(_, X, Y, theta, lambda);

            theta = theta - (alpha * grad);

            J_History(i) = cost;

            if debug
                ioPrintln('Gradient descent iteration: %d %f', i, cost);
            end
        end

    else
        % Optimization function
        options = optimset('GradObj', 'on', 'MaxIter', numIterations);
        [theta, J, exit_flag] = ...
            fminunc(@(t)(sCostFunction(_, X, Y, t, lambda)), theta, options);

end
