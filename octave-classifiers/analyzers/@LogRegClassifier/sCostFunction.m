function [J, grad] = sCostFunction(_, X, Y, theta, lambda)
    m = length(Y); % number of training examples

    hypothesis = sCalculateSigmoid(_, X * theta);

    J = (sum(((-Y .* log(hypothesis)) - ((ones(m, 1) - Y) .* log(ones(m, 1) - hypothesis)))) ./ m) + ((lambda / (2 * m)) * sum(theta(2:size(theta),:).^2));

    grad = ctranspose((ctranspose(hypothesis - Y) * X) ./ m) + [0; (lambda / m) * theta(2:size(theta), :)];

end
