function count = getValidationSetRowCount(c)
    count = size(c.xVal, 1);
end
