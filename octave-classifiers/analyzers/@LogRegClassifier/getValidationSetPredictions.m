function predictions = getValidationSetPredictions(c)
    predictions = sCalculatePredictions(c, c.xVal, c.theta);
end
