function count = getTrainingSetRowCount(c)
    count = size(c.xTrain, 1);
end
