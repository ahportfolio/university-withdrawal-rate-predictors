function predictions = getTestSetPredictions(c)
    predictions = sCalculatePredictions(c, c.xTest, c.theta);
end
