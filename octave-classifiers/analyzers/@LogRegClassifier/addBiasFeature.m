function c = addBiasFeature(c)
    c.xTrain = [ones(size(c.xTrain, 1), 1), c.xTrain];

    c.xVal = [ones(size(c.xVal, 1), 1), c.xVal];

    c.xTest = [ones(size(c.xTest, 1), 1), c.xTest];

    c._biasAdded = true;
end
