function [c, terms] = expandFeatures(c, maxDegree)
    [c.xTrain, c._builtTerms] = utilBuildPolynomialTerms(c.xTrain, maxDegree);

    c.xVal = c.xTrain;
    c.xTest = c.xTrain;

    terms = c._builtTerms;

    c._featureExpansionDegree = maxDegree;
end
