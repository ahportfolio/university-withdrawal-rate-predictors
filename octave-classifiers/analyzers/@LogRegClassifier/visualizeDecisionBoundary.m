function visualizeDecisionBoundary(c)
    % PLOTDECISIONBOUNDARY Plots the data points X and Y into a new figure with
    % the decision boundary defined by theta
    %   PLOTDECISIONBOUNDARY(theta, X,Y) plots the data points with + for the
    %   positive examples and o for the negative examples. X is assumed to be
    %   a either
    %   1) Mx3 matrix, where the first column is an all-ones column for the
    %      intercept.
    %   2) MxN, N>3 matrix, where the first column is all-ones

    % Plot Data
    theta = c.theta;

    U = visualizeData(c);

    hold on;

    if size(theta, 1) <= 3
        % Only need 2 points to define a line, so choose two endpoints
        if c._biasAdded
            plot_x = [min(c.xTrain(:,2))-2,  max(c.xTrain(:,2))+2];
        else
            plot_x = [min(c.xTrain(:,1))-2,  max(c.xTrain(:,1))+2];
        end
        % Calculate the decision boundary line
        plot_y = (-1./theta(3)).*(theta(2).*plot_x + theta(1));

        % Plot, and adjust axes for better viewing
        plot(plot_x, plot_y);
    else
        % Here is the grid range
        u = linspace(-1, 1.5, 50);
        v = linspace(-1, 1.5, 50);

        z = zeros(length(u), length(v));

        ioPrint("Calculating decision boundary: ");
        % Evaluate z = theta * x over the grid
        for i = 1:length(u)

            percentage = (i / length(u)) * 100;
            ioPrint(' %d%s', percentage, '%%');
            for j = 1:length(v)
                mappedVector = [u(i), v(j)];
                expandedVector = utilPCAExpansion(mappedVector, U);
                if c._featureExpansionDegree > -1
                    z(i,j) = utilBuildPolynomialTerms([ones(size(u, 1), 1), expandedVector], c._featureExpansionDegree) * theta;
                else
                    z(i,j) = [ones(size(u, 1), 1), expandedVector] * theta;
                end
            end
        end
        ioPrint("\n");

        z = ctranspose(z); % important to transpose z before calling contour

        % Plot z = 0
        % Notice you need to specify the range [0, 0]
        contour(u, v, z, [0, 0], 'LineWidth', 2)
        drawnow;
    end

    hold off;

end
