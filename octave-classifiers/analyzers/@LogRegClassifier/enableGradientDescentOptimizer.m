function c = enableGradientDescentOptimizer(c, numIter = 15000, learningRate = 0.01)
    c._optimizer = 'grad-desc';
    c._optimizerOptions.maxIters = numIter;
    c._optimizerOptions.learningRate = learningRate;
end
