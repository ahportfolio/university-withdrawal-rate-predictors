function U = visualizeData(c)

    if c._biasAdded
        xOriginal = c.xTrain(:, 2:size(c.xOriginal, 2) + 1);
    else
        xOriginal = c.xTrain(:, 1:size(c.xOriginal, 2));
    end
    yOriginal = c.yTrain;

    % Create New Figure
    figure; hold on;

    n = size(xOriginal, 2);

    if n > 2
        [X, U] = utilPCAReduce(xOriginal, 2);
        Y = yOriginal;
        title(sprintf('Dataset visualization mapped to 2D from %dD', n));

        ylabel('Z1');
        xlabel('Z2');
    else
        title(sprintf('Dataset visualization'));

        U = eye(n);

        X = xOriginal;
        Y = yOriginal;


        ylabel('X1');
        xlabel('X2');
    end


    markerConfigs = {'+k', 'or'};
    numConfigs = size(markerConfigs, 2);

    classes = ctranspose(unique(Y(:)));
    i = 0;
    for class = classes
        foundInstances = find(Y == class);

        plot(X(foundInstances, 1), X(foundInstances, 2), markerConfigs(mod(i,numConfigs) + 1), 'markersize', 2);

        i += 1;
    end

    legendValues = {1:size(classes, 2)};
    for i = 1:size(classes, 2)
        legendValues(i) = ['Value is ' int2str(classes(i)), '  '];
    end

    legend(legendValues);
    hold off;
    drawnow;
end
