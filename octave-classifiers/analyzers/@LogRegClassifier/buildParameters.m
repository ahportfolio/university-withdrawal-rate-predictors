function c = buildParameters(c, debug=false)

    if !isfield(c, 'theta')
        % Initialize fitting parameters
        c.theta = zeros(size(c.xTrain, 2), 1);
    end


    [c.theta, c._costHistory] = sCalculateParameters(c, c.xTrain, c.yTrain, c.theta, c._optimizer, c._optimizerOptions, debug);

end
