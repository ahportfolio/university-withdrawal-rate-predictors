function predictions = sCalculatePredictions(_, X, theta)
    predictions = sCalculateSigmoid(_, X * theta) > 0.5;
end
