function visualizeLearningCurve(c)

	m = size(c.xTrain, 1);
	n = size(c.xTrain, 2);

	error_train = zeros(m, 1);
	error_val   = zeros(m, 1);

	lambda = c._optimizerOptions.regularizationFactor;

	for i = 1:m
		trainedTheta = sCalculateParameters(c, c.xTrain(1:i, :), c.yTrain(1:i), zeros(n, 1), c._optimizer, c._optimizerOptions);
		[error_train(i), gradient] = sCostFunction(c, c.xTrain(1:i, :), c.yTrain(1:i), trainedTheta, lambda);
		[error_val(i), gradient] = sCostFunction(c, c.xVal, c.yVal, trainedTheta, lambda);
	end


	figure;
	plot(1:m, error_train, 1:m, error_val);
	ylim([0,1]);

	title(sprintf('Learning Curve (lambda = %f)', lambda));

	xlabel('Number of training examples')
	ylabel('Cost')

	legend('Train	', 'Cross Validation	');

	drawnow;
end
