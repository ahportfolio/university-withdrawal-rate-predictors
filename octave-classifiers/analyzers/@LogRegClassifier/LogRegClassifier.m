function c = LogRegClassifier(X, Y)
    c.xOriginal = X;
    c.yOriginal = Y;

    c.xTrain = X;
    c.yTrain = Y;

    c.xVal = X;
    c.yVal = Y;

    c.xTest = X;
    c.yTest = Y;

    c._featureExpansionDegree = -1;
    c._biasAdded = false;

    c._optimizer = 'fminunc';
    c._optimizerOptions.maxIters = 400;
    c._optimizerOptions.regularizationFactor = 0.3;

    c = class(c, "LogRegClassifier");
end
