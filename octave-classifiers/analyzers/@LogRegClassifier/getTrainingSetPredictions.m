function predictions = getTrainingSetPredictions(c)
    predictions = sCalculatePredictions(c, c.xTrain, c.theta);
end
