function count = getTestSetRowCount(c)
    count = size(c.xTest, 1);
end
