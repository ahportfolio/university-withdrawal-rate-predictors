function ratings = metricsCalculateFeatureRatings(X, Y, i=0)
    ratings = zeros(1, size(X, 2));

    if i > 0
        [C1, B1] = hist(X(find(Y==0), i));
        [C2, B2] = hist(X(find(Y==1), i), B1);

        ratings = sum(abs(C1 - C2)) / (2 * size(X, 1));
    else
        for i = 1:size(X, 2)
            [C1, B1] = hist(X(find(Y==0), i));
            [C2, B2] = hist(X(find(Y==1), i), B1);

            ratings(1, i) = sum(abs(C1 - C2)) / (2 * size(X, 1));
        end
    end


end

mean


