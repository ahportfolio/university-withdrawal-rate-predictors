function distortion = metricsCalculateDistortion(X, centroid)
	m = size(X, 1);
    distortion = sum(sum((X - repmat(ctranspose(centroid), m, 1)) .^ 2, 2)) ./ m;
end
