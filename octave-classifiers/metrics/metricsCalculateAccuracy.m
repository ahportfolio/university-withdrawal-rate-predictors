function [accuracy] = metricsCalculateAccuracy(predicted, Y)

    tc = sum(predicted == Y);
    t = length(Y);

    accuracy = tc/t;
end
