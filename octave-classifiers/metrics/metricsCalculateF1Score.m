function [F1, prec, rec, tp, fp, tn, fn] = metricsCalculateF1Score(predicted, Y)

    tp = sum(bitand(Y, predicted));
	fp = sum(bitand(not(Y), predicted));
	tn = sum(bitand(not(Y), not(predicted)));
	fn = sum(bitand(Y, not(predicted)));


	if (tp != 0)
		prec = tp / (tp + fp);
		rec = tp / (tp + fn);

		F1 = (2 * prec * rec) / (prec + rec);
	else
		prec = 0;

		rec = 0;

		F1 = 0;
	end;

end
