function [X, Y] = loadData()
    X = csvread('../data/class-1-fixed-features.csv')(2:end, :);
    Y = csvread('../data/class-1-fixed-targets.csv')(2:end, :);
end
