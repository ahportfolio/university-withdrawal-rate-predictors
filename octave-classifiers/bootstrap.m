function bootstrap()
    addpath ("./analyzers");
    addpath ("./debug");
    addpath ("./io");
    addpath ("./metrics");
    addpath ("./optimizers");
    addpath ("./util");
end
