function utilVisualizeFeatureComparison(X, Y, i, mode = 0)

    if mode == -1
        mNeg = size(X(find(Y==0)), 1);
        mPos = size(X(find(Y==1)), 1);
        negOnesCount = sum(X(find(Y==0), i));
        posOnesCount = sum(X(find(Y==1), i));

        bar([negOnesCount, (mNeg - negOnesCount); posOnesCount, (mPos - posOnesCount)]);
        set(gca, 'XTick', 1:2);
    else
        hist(X(find(Y==0), i));
        hold on;
        hist(X(find(Y==1), i), "facecolor", "r");

        legend("Value = 0   ", "Vale = 1    ");
        hold off;
    end
end
