function [mappedX, U_reduce] = utilPCAReduce(X, dimension, U_reduce = false)
    m = size(X, 1);

    if U_reduce == false
        covarienceMatrix = (ctranspose(X) * X)/m;
        [U, S, V] = svd(covarienceMatrix);

        U_reduce = U(:, 1:dimension);
    end
    mappedX = X * U_reduce;

end
