function utilDumpClusterEvalMetrics(cluster, centroid)
    distortion = metricsCalculateDistortion(cluster, centroid);

    ioPrintln('Distortion:  %f', distortion);
end
