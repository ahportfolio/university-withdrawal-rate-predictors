function utilDumpPredictionEvalMetrics(yPredicted, yTrain)
    [F1, prec, rec, tp, fp, tn, fn] = metricsCalculateF1Score(yPredicted, yTrain);
    accuracy = metricsCalculateAccuracy(yPredicted, yTrain);

    ioPrintln('True Positives:  %d', tp);
    ioPrintln('False Positives: %d', fp);
    ioPrintln('True Negatives:  %d', tn);
    ioPrintln('False Negatives: %d', fn);
    ioPrintln('Precision:       %f', prec);
    ioPrintln('Recall:          %f', rec);
    ioPrintln('F1 Score:        %f', F1);
    ioPrintln('Accuracy:        %f', accuracy);

end
