function X = utilPCAExpansion(mappedX, U)
    X = mappedX * ctranspose(U);
end
