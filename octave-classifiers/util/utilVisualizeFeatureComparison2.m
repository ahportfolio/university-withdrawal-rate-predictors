function utilVisualizeFeatureComparison(X, Y, i, mode = 0)

    if mode == -1
        mNeg = size(X(find(Y==0)), 1);
        mPos = size(X(find(Y==1)), 1);
        negOnesCount = sum(X(find(Y==0), i));
        posOnesCount = sum(X(find(Y==1), i));

        bar([negOnesCount, (mNeg - negOnesCount); posOnesCount, (mPos - posOnesCount)]);
        set(gca, 'XTick', 1:2);
    else
        %hist(X(find(Y==0), i));
        %hold on;
        %hist(X(find(Y==1), i), "facecolor", "r");

        %legend("Value = 0   ", "Vale = 1    ");
        %hold off;




       [y1 x1] = hist(X(find(Y==0), i));
       [y2 x2] =  hist(X(find(Y==1), i), "facecolor", "r");

       [ys1 xs1] = stairs(y1, x1);
       [ys2 xs2] = stairs(y2, x2);

       xs1 = [xs1(1); xs1; xs1(end)];  xs2 = [xs2(1); xs2; xs2(end)];
       ys1 = [0; ys1; 0];  ys2 = [0; ys2; 0];

       clf
       hold on;
       h1=fill(xs1,ys1,"red");
       h2=fill(xs2,ys2,"green");

        set(h1,'facealpha',0.5);
        set(h2,'facealpha',0.5);
        hold off;

        legend("Value = 0   ", "Vale = 1    ");
        hold off;
    end
end
