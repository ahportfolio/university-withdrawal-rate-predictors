function [out, newPolynomialRepr] = utilBuildPolynomialTerms(X, degree)
    % Feature mapping function to polynomial features
    %
    %   Returns a new feature array with more features, comprising of
    %   X1, X2, X1^2, X2^2, X1*X2, X1*X2^2, etc..
    %
    m = size(X, 1);
    n = size(X, 2);

    out = [];

    function repr = buildFactorRepr(idx, exp)
        if exp != 0
            repr = ['(X' int2str(idx)];
            if i != 1
                repr = [repr '^' int2str(exp)];
            end
            repr = [repr ')'];
        else
            repr = '(1)';
        end;
    end

    newPolynomialRepr = '';

    numTerms = 0;
    if n > 1
        for i = 1:degree
            for k = 1:n
                if !(i == 1 && k == 1)
                    newPolynomialRepr = [newPolynomialRepr " + \n"];
                end

                numTerms += 1;
                out = [out, (X(:,k).^i)];

                newPolynomialRepr = [newPolynomialRepr buildFactorRepr(k, i)];
            end

            for k = 1:(n - 1)
                numTerms += 1;
                newPolynomialRepr = [newPolynomialRepr " + \n"];

                out = [out, ((X(:,k).^i) .* (X(:,k + 1).^i))];

                newPolynomialRepr = [newPolynomialRepr buildFactorRepr(k, i)];
                newPolynomialRepr = [newPolynomialRepr buildFactorRepr(k + 1, i)];
            end


%            newPolynomialRepr = [newPolynomialRepr " + \n"];
%
%            newTerm = ones(m, 1);
%            currentIndexEndFactorIndex = max(0, ((n - i) + 1));
%            for k = 1:currentIndexEndFactorIndex
%                newTerm = newTerm .* (X(:,k).^i);
%
%                newPolynomialRepr = [newPolynomialRepr buildFactorRepr(k, i)];
%            end
%
%            j = 0;
%            for k = (currentIndexEndFactorIndex + 1):n
%                j += 1;
%
%                newTerm = newTerm .* (X(:,k).^(i - j));
%
%                newPolynomialRepr = [newPolynomialRepr buildFactorRepr(k, (i - j))];
%            end
%
%            if newTerm == NaN
%                keyboard('debug(NaN)> ');
%            end
%            numTerms += 1;
%            out = [out, newTerm];
        end
    else
        for i = 1:degree
            if i != 1
                numTerms += 1;
                newPolynomialRepr = [newPolynomialRepr ' + ' ];
            end

            out = [out, (X(:,1).^i)];
            if i == 1
                newPolynomialRepr = [newPolynomialRepr 'X'];
            else
                newPolynomialRepr = [newPolynomialRepr 'X^' int2str(i)];
            end
        end
    end

    % disp(['Num terms: ', int2str(numTerms)]); % debug
end
