function M = utilBuildRandomMatrix(x, y)
    epsilon_init = 0.12;
    W = (rand(x, y) * (2 * epsilon_init)) - epsilon_init;
end
