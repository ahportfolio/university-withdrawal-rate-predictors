clear; close all; clc;

bootstrap();

function [K, X, Y, Labels] = loadCluster1Data()
    K = csvread('../data/cluster-1-identifiers.csv');
    X = csvread('../data/cluster-1-features.csv');
    Y = csvread('../data/cluster-1-targets.csv');

    K = K(2:end, :); % remove title row
    X = X(2:end, :); % remove title row
    Y = Y(2:end, :); % remove title row

%    load('ex7data2.mat');

end

[K, X, Y] = loadCluster1Data();

mOriginal = size(X, 1);
nOriginal = size(X, 2);

ioPrintln('Data imported: %d rows, %d features', mOriginal, nOriginal);

clusterBuilder = KMeansClusterBuilder(X, 3);

clusterBuilder = normalizeFeatures(clusterBuilder);

clusterBuilder = randomizeCentroids(clusterBuilder);

visualizeData(clusterBuilder);

clusterBuilder = optimizeCentroids(clusterBuilder, false);

visualizeData(clusterBuilder);

for i = 1:getClusterCount(clusterBuilder)
    clusterDataset = getClusterDataset(clusterBuilder, i);
    ioPrintln('\nDumping dataset %d: %d rows', i, size(clusterDataset, 1));
    utilDumpClusterEvalMetrics(clusterDataset, getClusterCentroid(clusterBuilder, i));

    utilDumpDataset(['./_output-dataset-clusters/dataset-cluster-' int2str(i) '.csv'], K, clusterDataset);
end
